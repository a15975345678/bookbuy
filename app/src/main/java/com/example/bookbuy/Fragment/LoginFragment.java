package com.example.bookbuy.Fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bookbuy.R;
import com.example.bookbuy.Service.UserLoginSystem;


public class LoginFragment extends DialogFragment {

    private EditText username;
    private EditText password;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View myView = inflater.inflate(R.layout.dialog_signin, null);
        username = (EditText) myView.findViewById(R.id.username);
        password = (EditText) myView.findViewById(R.id.password);
        builder.setView(myView)
            // Add action buttons
            .setPositiveButton(R.string.signin, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //Do nothing.
                }
            })
            .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //Do nothing.
//                        Toast.makeText(getContext(), "Cancel", Toast.LENGTH_SHORT).show();
                }
            })
            .setNeutralButton(R.string.clear, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
                    //Do nothing.
                }
            });
        return builder.create();
    }

    @Override
    public void onStart() {
        super.onStart();    //super.onStart() is where dialog.show() is actually called on the underlying dialog, so we have to do it after this point
        AlertDialog d = (AlertDialog) getDialog();
        if (d != null) {
            Button neutralButton = (Button) d.getButton(Dialog.BUTTON_NEUTRAL);
            neutralButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    username.setText("");
                    password.setText("");
                }
            });

            Button positiveButton = (Button) d.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean wantToCloseDialog = false;
                    //Do stuff, possibly set wantToCloseDialog to true then...
                    if (username.getText().toString().equals("user") && password.getText().toString().equals("user")) {
                        dismiss();
//                        View requireActivity().getLayoutInflater().inflate(R.layout.dialog_signin, null);
                        UserLoginSystem.loginIn();
                        Toast.makeText(getContext(), "Sign in succeed.", Toast.LENGTH_SHORT).show();
                    }else{
                        UserLoginSystem.loginOut();
                        Toast.makeText(getContext(), "Error! Please Enter \"user\"", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }
}