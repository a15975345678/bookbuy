package com.example.bookbuy.Activity;

import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.bookbuy.Fragment.ContactFragment;
import com.example.bookbuy.Fragment.CreditFragment;
import com.example.bookbuy.Fragment.InformationFragment;
import com.example.bookbuy.Fragment.LoginFragment;
import com.example.bookbuy.Fragment.OrderFragment;
import com.example.bookbuy.Fragment.ProductFragment;
import com.example.bookbuy.Fragment.SaleActivityFragment;
import com.example.bookbuy.Fragment.ShoppingCartFragment;
import com.example.bookbuy.R;
import com.example.bookbuy.Service.ShoppingCartSystem;
import com.example.bookbuy.Service.UserLoginSystem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;


public class MainActivity extends AppCompatActivity {

    private ShoppingCartSystem SCS;
    private UserLoginSystem ULS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 初始頁面設定
        loadFragment(new SaleActivityFragment());
        init_navDrawer();
        init_fabButton();
        init_toolbar();
        SCS = new ShoppingCartSystem(this);
        ULS = new UserLoginSystem();
    }


    private void init_toolbar() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        // 用toolbar做為APP的ActionBar
        setSupportActionBar(toolbar);

        // 將drawerLayout和toolbar整合，會出現「三」按鈕
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout, toolbar, R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        getSupportActionBar().setDisplayShowTitleEnabled(true);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.action_search:
                        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                        intent.putExtra(SearchManager.QUERY, "博客來書店"); // query contains search string
                        startActivity(intent);
                        break;
                    case R.id.action_shoppingcart:
                        loadFragment(new ShoppingCartFragment());
                        break;
                }

                return false;
            }
        });
    }

    private void init_fabButton() {

        FloatingActionButton fab = findViewById(R.id.button_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Open Map!", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=高第一");
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
    }

    private void init_navDrawer() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.View_navigation);


        // 為navigatin_view設置點擊事件
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                // 點選時收起選單
                drawerLayout.closeDrawer(GravityCompat.START);

                // 取得選項id
                int id = item.getItemId();
                Fragment fragment = null;


                // 依照選取的Item開啟對應的頁面
                switch (id) {
                    case R.id.productFragment:
                        fragment = new ProductFragment();
                        break;
                    case R.id.saleActivityFragment:
                        fragment = new SaleActivityFragment();
                        break;
                    case R.id.creditFragment:
                        fragment = new CreditFragment();
                        break;
                    case R.id.shoppingCarFragment:
                        fragment = new ShoppingCartFragment();
                        break;
                    case R.id.orderFragment:
                        fragment = new OrderFragment();
                        break;
                    case R.id.informationFragment:
                        fragment = new InformationFragment();
                        break;
                    case R.id.contactFragment:
                        fragment = new ContactFragment();
                        break;
                    case R.id.loginFragment:
                        DialogFragment newFragment = new LoginFragment();
                        newFragment.show(getSupportFragmentManager(), "missiles");
                        return false;
                }

                //切換頁面
                loadFragment(fragment);
                return true;
            }
        });


    }

    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.layout_frame, fragment).commit();
    }

    @Override
    public boolean onPrepareOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }


}