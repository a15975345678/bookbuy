package com.example.bookbuy.Service;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;

import com.example.bookbuy.R;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartSystem {

    public static  int[] cart;
    private String[] bookdata;
    private TypedArray images;

    public ShoppingCartSystem(Context context){
        bookdata = context.getResources().getStringArray(R.array.books);
        images = context.getResources().obtainTypedArray(R.array.bookimages);
        cart = new int[bookdata.length/2];
    }

    public static void AddtoCart(int pos){
        if (cart[pos] > 0){
            cart[pos] = 0;
        }else{
            cart[pos] = 1;
        }
    }
    public static void changeBookCount(int pos, int num){
        cart[pos] = cart[pos] + num;
    }
    public static int getCartCount(int pos){
        return cart[pos];
    }


}
