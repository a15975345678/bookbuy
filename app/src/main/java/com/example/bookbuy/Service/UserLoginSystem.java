package com.example.bookbuy.Service;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class UserLoginSystem extends AppCompatActivity {

    public static final int LOGINSTATUS_LOGGED = 1;
    public static final int LOGINSTATUS_NOTLOGGED = 0;

    public static int LoginStatus = LOGINSTATUS_NOTLOGGED;

    private static TextView loginTitle, loginSub;
    private static MenuItem loginFragmentTitle;

    public static void loginIn() {
//        loginFragmentTitle.setTitle("Logout");
//        loginTitle.setText("測試");
//        loginSub.setText("歡迎使用BookBuy購買書籍");
        LoginStatus = LOGINSTATUS_LOGGED;
    }

    public static void loginOut() {
//        loginFragmentTitle.setTitle("Login");
//        loginTitle.setText("Hello! User.");
        LoginStatus = LOGINSTATUS_NOTLOGGED;
    }

//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_main);
//        Toast.makeText(getApplicationContext(), "DONEEE", Toast.LENGTH_SHORT).show();
//        loginTitle = (TextView) findViewById(R.id.textView_loginTitle);
//        loginSub = (TextView) findViewById(R.id.textView_loginSub);
//        loginFragmentTitle = (MenuItem) findViewById(R.id.loginFragment);
//    }
}
