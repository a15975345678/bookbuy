package com.example.bookbuy.Service;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.bookbuy.Product;
import com.example.bookbuy.R;

import java.util.List;

public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingAdapter.ViewHolder> {
    private Context context;
    private List<Product> ShoppingAdapter;

    public ShoppingAdapter(Context context, List<Product> ShoppingAdapter) {
        this.context = context;
        this.ShoppingAdapter = ShoppingAdapter;
    }

    @Override
    public ShoppingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_shoppingcart, parent, false);
        return new ShoppingAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShoppingAdapter.ViewHolder holder, int position) {
        final Product Product = ShoppingAdapter.get(position);
        holder.imageId.setImageResource(Product.getImage());
        holder.textCount.setText(String.valueOf(ShoppingCartSystem.cart[Product.getId()]));
        holder.textName.setText(Product.getName());
        holder.imageAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = ShoppingCartSystem.cart[Product.getId()];
                ShoppingCartSystem.changeBookCount(Product.getId(), 1);
                holder.textCount.setText(String.valueOf(count + 1));
            }
        });
        holder.imageNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = ShoppingCartSystem.cart[Product.getId()];
                if (count > 0) {
                    ShoppingCartSystem.changeBookCount(Product.getId(), -1);
                    holder.textCount.setText(String.valueOf(count - 1));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return ShoppingAdapter.size();
    }

    //Adapter 需要一個 ViewHolder，只要實作它的 constructor 就好，保存起來的view會放在itemView裡面
    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageId, imageNeg, imageAdd;
        TextView textName, textCount;

        ViewHolder(View itemView) {
            super(itemView);
            imageId = (ImageView) itemView.findViewById(R.id.imageId);
            imageNeg = (ImageView) itemView.findViewById(R.id.imageView_neg);
            imageAdd = (ImageView) itemView.findViewById(R.id.imageView_add);
            textName = (TextView) itemView.findViewById(R.id.textName);
            textCount = (TextView) itemView.findViewById(R.id.textView_count);

        }
    }
}
