package com.example.bookbuy.Service;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.bookbuy.Product;
import com.example.bookbuy.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private Context context;
    private List<Product> ProductList;

    public ProductAdapter(Context context, List<Product> ProductList) {
        this.context = context;
        this.ProductList = ProductList;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cardview_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
        final Product Product = ProductList.get(position);
        if(ShoppingCartSystem.getCartCount(Product.getId()) > 0){
            holder.imageCheck.setImageResource(R.drawable.ic_baseline_radio_button_checked_24);
        }else{
            holder.imageCheck.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24);
        }
        holder.imageId.setImageResource(Product.getImage());
        holder.textName.setText(Product.getName());
        holder.textDescription.setText(Product.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = Product.getId();
                ShoppingCartSystem.AddtoCart(id);
                int count = ShoppingCartSystem.getCartCount(id);
                if(count > 0){
                    holder.imageCheck.setImageResource(R.drawable.ic_baseline_radio_button_checked_24);
                }else{
                    holder.imageCheck.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24);
                }
//                Toast.makeText(context, String.valueOf(count), Toast.LENGTH_SHORT).show();
//                ImageView imageView = new ImageView(context);
//                imageView.setImageResource(Product.getImage());
//                Toast toast = new Toast(context);
//                toast.setView(imageView);
//                toast.setDuration(Toast.LENGTH_SHORT);
//                toast.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return ProductList.size();
    }

    //Adapter 需要一個 ViewHolder，只要實作它的 constructor 就好，保存起來的view會放在itemView裡面
    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageId, imageCheck;
        TextView textDescription, textName;

        ViewHolder(View itemView) {
            super(itemView);
            imageId = (ImageView) itemView.findViewById(R.id.imageId);
            imageCheck = (ImageView) itemView.findViewById(R.id.imageCheck);
            textDescription = (TextView) itemView.findViewById(R.id.textDescription);
            textName = (TextView) itemView.findViewById(R.id.textName);

        }
    }
}
